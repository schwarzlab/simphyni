import argparse
import os

import Bio.Phylo

from simphyni.simulate import simulate_cn_tree

parser = argparse.ArgumentParser()
parser.add_argument("--n-leafs", "-N",
                    required=True,
                    type=int,
                    dest="n_leafs",
                    help="""Number of leafs""")
parser.add_argument("--output-dir", "-o",
                    required=True,
                    type=str,
                    dest="output_dir",
                    help="a path to the output folder")
parser.add_argument("--name",
                    required=False,
                    type=str,
                    default='simulated',
                    help="Name of the output")
args = parser.parse_args()

tree, final_df, all_events = simulate_cn_tree(args.n_leafs, rate=0.02, p_geom=0.25)

# Write output
Bio.Phylo.write(tree, os.path.join(args.output_dir, args.name + "_tree.new"), "newick")
final_df.to_csv(os.path.join(args.output_dir, args.name + "_final_df.tsv"), sep='\t')
