# Simphyni

**Sim**ulations for **Phy**logenetic copy **n**umbers **i**n cancer

Used for benchmarking of phylogenetic reconstruction tools in [MEDICC2: whole-genome doubling aware copy-number phylogenies for cancer evolution](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-022-02794-9).


# How to run
Example call from terminal for a tree with 10 leafs:
`python simphyni.py -N 10 -o path/to/ouput --name example_name`

Call from another python script:
```python
from simphyni import simulate_cn_tree

tree, final_df, all_events = simulate_cn_tree(10)
```

See also the Jupyter Notebook in `notebooks/create_trees_for_MEDICC2.ipynb` which contains the script that created the trees for the benchmarking in the MEDICC2 publication.

## Dependencies

To install dependencies using pip, run the following command: 
```
pip install numpy pandas PyYAML biopython
```

# Algorithm Details

## Tree
A tree is created by randomly joining the N leafs to create a given tree topology. All branch lengths (which are equal to the evolutionary timing $\Delta t$) are set to $1$. The number of events per branch are chosen based on a Poisson distribution with $\lambda = \Delta t \cdot \mu \cdot L$ with event rate $\mu$ and number of segments $L$ ($L$ is fixed by default to the size of the diploid).

## Events
Supported copy number abberations are:

* chromosomal gains
* chromosomal losses
* tandem duplication
* focal losses
* unbalanced translocation (cut-paste)
* balanced translocation (exchange of two events)
* insertions (copy-paste)
* whole-genome doubling
* breakage-fusion-bridge
* inversions


# TODO
* add conda env
* make parameters `rate` and `p_geom` accessible via argparse
* load event probabilies from file
* create trees with variable branch lengths / evolutionary times


# Contact
Email questions, feature requests and bug reports to **Tom Kaufmann, tom.kaufmann@mdc-berlin.de**.


# Usage
If you use this software in your work please cite the MEDICC2 publication:

Kaufmann, T.L., Petkovic, M., Watkins, T.B.K. et al.  
**MEDICC2: whole-genome doubling aware copy-number phylogenies for cancer evolution**.  
Genome Biol 23, 241 (2022). https://doi.org/10.1186/s13059-022-02794-9
