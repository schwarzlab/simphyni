import copy
import logging

import numpy as np

logger = logging.getLogger(__name__)


def chromosomal_gain(cn, p_geom=None):
    cur_chrom = np.random.randint(0, len(cn))
    logger.debug('Chromosomal gain on chrom index {}'.format(cur_chrom))
    return cn + [cn[cur_chrom]]


def chromosomal_loss(cn, p_geom=None):
    _cn = copy.deepcopy(cn)
    cur_chrom = np.random.randint(0, len(_cn))
    if 'b' not in [x[-1] for x in _cn[cur_chrom]]:
        logger.debug('Chromosomal loss on chrom index {}'.format(cur_chrom))
        _cn.pop(cur_chrom)
    return _cn


def tandem_duplication(cn, p_geom=0.25):

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        start = 0
        length = 1
    else:
        start = np.random.randint(0, len(cur_cn)-1)
        max_length = len(cur_cn) - start - 1
        length = min(np.random.geometric(p_geom), max_length)

    # now insert again
    cur_cn = cur_cn[:start] + 2*cur_cn[start:start+length] + cur_cn[start:]
    cn = cn[:chrom] + [cur_cn] + cn[chrom+1:]
    logger.debug('Tandem duplication on chrom index {} at pos {}-{}'.format(chrom, start,
                                                                            start+length))
    return cn


def insertion(cn, p_geom=0.25):
    '''copy and paste event'''

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        start = 0
        length = 1
    else:
        start = np.random.randint(0, len(cur_cn)-1)
        max_length = len(cur_cn) - start - 1
        length = min(np.random.geometric(p_geom), max_length)

    copied_cn = cur_cn[start:start+length]

    # now insert again
    new_chrom = np.random.randint(0, len(cn))
    cur_cn = cn[new_chrom]

    new_start = np.random.randint(0, len(cur_cn)+1)

    cur_cn = cur_cn[:new_start] + copied_cn + cur_cn[new_start:]
    cn = cn[:new_chrom] + [cur_cn] + cn[new_chrom+1:]
    logger.debug('Insertion from chrom index {} at pos {}-{}\n'
                 'to chrom index {} at pos {}'.format(chrom, start, start+length, new_chrom, new_start))
    return cn


def focal_loss(cn, p_geom=0.25):

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        start = 0
        length = 1
    else:
        start = np.random.randint(0, len(cur_cn)-1)
        max_length = len(cur_cn) - start - 1
        length = min(np.random.geometric(p_geom), max_length)

    if 'b' not in [x[-1] for x in cur_cn[start:start+length]]:
        cur_cn = cur_cn[:start] + cur_cn[start+length:]
        cn = cn[:chrom] + [cur_cn] + cn[chrom+1:]

    logger.debug('Focal loss on chrom index {} at pos {}-{}'.format(chrom, start, start+length))
    return cn


def wgd(cn, p_geom=None):

    logger.debug('Whole genome doubling')
    return 2*cn


def inversion(cn, p_geom=0.25):

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        start = 0
        length = 1
    else:
        start = np.random.randint(0, len(cur_cn)-1)
        max_length = len(cur_cn) - start - 1
        length = min(np.random.geometric(p_geom), max_length)

    cur_cn = cur_cn[:start] + cur_cn[start:start+length][::-1] + cur_cn[start+length:]

    cn = cn[:chrom] + [cur_cn] + cn[chrom+1:]

    logger.debug('Inversion on chrom index {} at pos {}-{}'.format(chrom, start, start+length))
    return cn


def unbalanced_translocation(cn, p_geom=0.25):

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        start = 0
        length = 1
    else:
        start = np.random.randint(0, len(cur_cn)-1)
        max_length = len(cur_cn) - start - 1
        length = min(np.random.geometric(p_geom), max_length)

    translocated_cn = cur_cn[start:start+length]
    cur_cn = cur_cn[:start] + cur_cn[start+length:]

    cn = cn[:chrom] + [cur_cn] + cn[chrom+1:]

    # now insert again
    new_chrom = np.random.randint(0, len(cn))
    cur_cn = cn[new_chrom]

    new_start = np.random.randint(0, len(cur_cn)+1)

    cur_cn = cur_cn[:new_start] + translocated_cn + cur_cn[new_start:]

    cn = cn[:new_chrom] + [cur_cn] + cn[new_chrom+1:]
    logger.debug('Unbalanced translocation from chrom index {} at pos {}-{}\n'
                 'to chrom index {} at pos {}'.format(chrom, start, start+length, new_chrom, new_start))
    return cn


def balanced_translocation(cn, p_geom=0.25):

    chrom_1 = np.random.randint(0, len(cn))

    # only one chromosome left
    if not len(np.setdiff1d(np.arange(len(cn)), [chrom_1])):
        return cn
    chrom_2 = np.random.choice(np.setdiff1d(np.arange(len(cn)), [chrom_1]))

    cur_cn_1 = cn[chrom_1]
    cur_cn_2 = cn[chrom_2]

    if len(cur_cn_1) == 1:
        start_1 = 0
        length_1 = 1
    else:
        start_1 = np.random.randint(0, len(cur_cn_1)-1)
        max_length = len(cur_cn_1) - start_1 - 1
        length_1 = min(np.random.geometric(p_geom), max_length)
    translocated_cn_1 = cur_cn_1[start_1:start_1+length_1]

    if len(cur_cn_2) == 1:
        start_2 = 0
        length_2 = 1
    else:
        start_2 = np.random.randint(0, len(cur_cn_2)-1)
        max_length = len(cur_cn_2) - start_2 - 1
        length_2 = min(np.random.geometric(p_geom), max_length)
    translocated_cn_2 = cur_cn_2[start_2:start_2+length_2]

    cur_cn_1 = cur_cn_1[:start_1] + translocated_cn_2 + cur_cn_1[start_1+length_1:]

    cur_cn_2 = cur_cn_2[:start_2] + translocated_cn_1 + cur_cn_2[start_2+length_2:]

    cn = [x for y, x in enumerate(cn) if y not in [chrom_1, chrom_2]] + [cur_cn_1, cur_cn_2]
    logger.debug('Balanced translocation with chrom index {} at pos {}-{}\n'
                 'and chrom index {} at pos {}-{}'.format(chrom_1, start_1, start_1+length_1,
                                                          chrom_2, start_2, start_2+length_2))
    return cn


def breakage_fusion_bridge(cn, p_geom=None):

    chrom = np.random.randint(0, len(cn))
    cur_cn = cn[chrom]

    if len(cur_cn) == 1:
        return cn

    breakpoint = np.random.randint(1, len(cur_cn))

    if 'b' not in [x[-1] for x in cur_cn[breakpoint:]]:
        cur_cn = cur_cn[:breakpoint] + cur_cn[:breakpoint][::-1]

        cn = cn[:chrom] + [cur_cn] + cn[chrom+1:]
        logger.debug('Breakage fusion bridge on chrom index {} at pos {}'.format(chrom, breakpoint))
    return cn
