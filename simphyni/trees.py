import logging

import Bio.Phylo
import numpy as np

logger = logging.getLogger(__name__)


def generate_tree(n_leafs):
    """Generates random tree topologies by joining the leafs. All branch lengths are equal to 1."""

    logger.info('Generating random tree with {} leafs'.format(n_leafs))

    sample_names = ['sample_{}'.format(i) for i in range(1, n_leafs+1)]
    samples = [Bio.Phylo.Newick.Clade(branch_length=1, name=sample) for sample in sample_names]

    internal_counter = 1

    while len(samples) > 1:
        child_1_ind, child_2_ind = np.random.choice(len(samples), replace=False, size=2)
        child_1 = samples[child_1_ind]
        child_2 = samples[child_2_ind]
        samples.remove(child_1)
        samples.remove(child_2)

        clade_internal = Bio.Phylo.Newick.Clade(
            branch_length=1, name='internal_{}'.format(internal_counter))

        logger.debug('Joining clades {} and {} to ancestor {}'.format(child_1.name, 
                                                                      child_2.name,
                                                                      clade_internal.name))

        samples.append(clade_internal)
        internal_counter += 1

        clade_internal.clades = [child_1, child_2]

    clade_root = Bio.Phylo.Newick.Clade(branch_length=0, name=None)
    clade_diploid = Bio.Phylo.Newick.Clade(branch_length=0, name='diploid')
    clade_root.clades = [clade_diploid, samples[0]]
    tree = Bio.Phylo.PhyloXML.Phylogeny(root=clade_root)

    return tree
