from dataclasses import dataclass

import numpy as np
import pytest

import simphyni


@dataclass
class Event:
    name: str
    expected_cn_change: int
    expected_chrom_change: int


EVENTS = [
    Event('chromosomal_gain', 1, 1),
    Event('chromosomal_loss', -1, -1),
    Event('focal_loss', -1, 0),
    Event('tandem_duplication', 1, 0),
    Event('unbalanced_translocation', 0, 0),
    Event('balanced_translocation', 0, 0),
    Event('insertion', 1, 0),
    Event('wgd', 1, 40),
    Event('inversion', 0, 0),
    Event('breakage_fusion_bridge', None, 0)
]


@pytest.mark.parametrize("n_leafs", np.arange(2, 20))
def test_whole_method(n_leafs: int):
    tree, final_df, all_events = simphyni.simulate.simulate_cn_tree(n_leafs)

    # Number of leafs
    assert len(tree.get_terminals()) == n_leafs + 1, 'Wrong number of leaf nodes in tree'
    assert len([x for x in np.unique(final_df.index.get_level_values('sample_id')) if 'internal' not in x]
               ) == n_leafs + 1, 'Wrong number of leaf samples in final_df'
    assert len([x for x in all_events.keys() if 'internal' not in x]
               ) == n_leafs, 'Wrong number of leaf samples in all_events'


@pytest.mark.parametrize("event", EVENTS)
def test_cn_change(event: Event, N=10):

    for _ in range(N):
        diploid = simphyni.tools.create_diploid(
            n_chroms=20, n_segs_per_chrom=10, separate_haplotypes=True)

        changed_cn = simphyni.evolve.apply_events(diploid, [event.name], p_geom=0.25)

        # Test change of chromosomes
        assert len(changed_cn) == len(diploid) + event.expected_chrom_change

        final_cn_profiles = simphyni.tools.cn_profile_from_simulation(
            [changed_cn], diploid, names=['test'], cap_maxcn=None)
        # Test direction of CN change
        if event.expected_cn_change is not None:
            assert np.sign((final_cn_profiles.loc['test'] -
                            final_cn_profiles.loc['diploid']).sum().sum()) == np.sign(event.expected_cn_change)

