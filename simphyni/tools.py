import logging
import re

import numpy as np
import pandas as pd
from numba import jit

logger = logging.getLogger(__name__)


def create_diploid(n_chroms=22, n_segs_per_chrom=10, separate_haplotypes=True):
    if separate_haplotypes:
        logger.info('Create diploid with {} chromosomes, {} segments per chromosomes and 2 haplotypes. Total {} segments'.format(n_chroms, n_segs_per_chrom, n_chroms*n_segs_per_chrom*2))
        diploid = [['{}-{}a'.format(chrom, seg) for seg in range(0, n_segs_per_chrom)] 
                      for chrom in range(1, n_chroms+1)] + \
                  [['{}-{}b'.format(chrom, seg) for seg in range(0, n_segs_per_chrom)]
                      for chrom in range(1, n_chroms+1)]

    else:
        logger.info('Create diploid with {} chromosomes, {} segments per chromosomes '
                    'and 1 haplotype. Total {} segments'.format(n_chroms, n_segs_per_chrom, n_chroms*n_segs_per_chrom))
        diploid = [['{}-{}'.format(chrom, seg) for seg in range(1, n_segs_per_chrom+1)]
                   for chrom in range(1, n_chroms+1)]
    return diploid


def cn_profile_from_simulation_single_haplotype(cns, diploid, names=['sim_cn'], cap_maxcn=8):

    assert len(cns) == len(names), '{}, {}'.format(len(cns), len(names))

    final_df = pd.DataFrame(columns=['chrom', 'start', 'diploid_cn'])
    final_df['chrom'] = [x.split('-')[0] for x in np.concatenate(diploid)]
    final_df['start'] = [x.split('-')[1] for x in np.concatenate(diploid)]
    final_df['start'] = final_df['start'].astype(int)
    final_df['end'] = final_df['start'] + 1
    final_df['diploid'] = 1
    final_df.set_index(['chrom', 'start', 'end'], inplace=True)

    for cn, name in zip(cns, names):

        segments, segment_count = np.unique(np.concatenate(cn), return_counts=True)

        cur_df = pd.DataFrame(columns=['chrom', 'start'])
        cur_df['chrom'] = [x.split('-')[0] for x in segments]
        cur_df['start'] = [x.split('-')[1] for x in segments]
        cur_df['start'] = cur_df['start'].astype(int)
        cur_df['end'] = cur_df['start'] + 1
        cur_df[name] = segment_count
        cur_df.set_index(['chrom', 'start', 'end'], inplace=True)

        final_df = final_df.join(cur_df).fillna(0).astype(int)

    final_df.columns.name = 'sample_id'
    final_df = pd.DataFrame(final_df.stack('sample_id'))
    final_df = final_df.reorder_levels(['sample_id', 'chrom', 'start', 'end']).sort_index()
    final_df.columns = ['cn_a']

    if cap_maxcn is not None:
        final_df['cn_a'] = np.fmin(final_df['cn_a'], cap_maxcn)
        final_df['cn_a'] = np.fmin(final_df['cn_a'], cap_maxcn)

    return final_df


# TODO: merge these two functions into one
# @jit()
def cn_profile_from_simulation(cns, diploid, names=['sim_cn'], cap_maxcn=8):

    assert len(cns) == len(names), '{}, {}'.format(len(cns), len(names))

    final_df = pd.DataFrame(columns=['chrom', 'start'])
    final_df['chrom'] = [x.split('-')[0] for x in np.concatenate(diploid) if 'a' in x]
    final_df['start'] = [x.split('-')[1].replace('a', '') for x in np.concatenate(diploid) if 'a' in x]
    final_df['start'] = final_df['start'].astype(int)
    final_df['end'] = final_df['start'] + 1
    final_df.set_index(['chrom', 'start', 'end'], inplace=True)
    final_df['cn_a'] = 1
    final_df['cn_b'] = 1
    final_df.columns = pd.MultiIndex.from_product([["cn_a", "cn_b"], ["diploid"]])
    final_df.columns.names = ['allele', 'sample_id']

    resulting_df = []
    for cn, name in zip(cns, names):

        for allele in ['a', 'b']:
            if len(cn) == 0:
                final_df[('cn_{}'.format(allele), name)] = 0
            else:
                segments, segment_count = np.unique(
                    [x.replace(allele, '') for x in np.concatenate(cn) if x[-1] == allele], return_counts=True)

                cur_df = pd.DataFrame(columns=['chrom', 'start'])
                cur_df['chrom'] = [x.split('-')[0] for x in segments]
                cur_df['start'] = [x.split('-')[1] for x in segments]
                cur_df['start'] = cur_df['start'].astype(int)
                cur_df['end'] = cur_df['start'] + 1
                cur_df.set_index(['chrom', 'start', 'end'], inplace=True)
                cur_df['cn_{}'.format(allele)] = segment_count
                cur_df.columns = pd.MultiIndex.from_product([["cn_{}".format(allele)], [name]])
                cur_df.columns.names = ['allele', 'sample_id']

                resulting_df.append(cur_df)

    final_df = pd.concat([final_df] + resulting_df, axis=1).fillna(0).astype(int)

    final_df = final_df.stack('sample_id')
    final_df = final_df.reset_index()
    final_df['chrom'] = format_chromosomes(final_df['chrom'])
    final_df = final_df.set_index(['sample_id', 'chrom', 'start', 'end']).sort_index()
    final_df = final_df[~final_df.index.duplicated(keep='first')]

    if cap_maxcn is not None:
        final_df['cn_a'] = np.fmin(final_df['cn_a'], cap_maxcn)
        final_df['cn_a'] = np.fmin(final_df['cn_a'], cap_maxcn)

    return final_df


def format_chromosomes(ds):
    """ Expects pandas Series with chromosome names. 
    The goal is to take recognisalbe chromosome names, i.e. chr4 or chrom3 and turn them into chr3 format.
    If the chromosomes names are not recognized, return them unchanged.
    
    Copied from MEDICC2, see https://bitbucket.org/schwarzlab/medicc2"""
    ds = ds.astype('str')
    pattern = re.compile(r"(chr|chrom)?((\d+)|X|Y)", flags=re.IGNORECASE)
    matches = ds.apply(pattern.match)
    matchable = ~matches.isnull().any()
    if matchable:
        newchr = matches.apply(lambda x: "chr%s" % x[2].upper())
        numchr = matches.apply(lambda x: int(x[3]) if x[3] is not None else -1)
        chrlevels = np.sort(numchr.unique())
        chrlevels = np.setdiff1d(chrlevels, [-1])
        chrcats = ["chr%d" % i for i in chrlevels]
        if 'chrX' in list(newchr):
            chrcats += ['chrX', ]
        if 'chrY' in list(newchr):
            chrcats += ['chrY', ]
        newchr = pd.Categorical(newchr, categories=chrcats)
    else:
        logger = logging.getLogger('medicc.io')
        logger.warn("Could not match the chromosome labels. Rename the chromosomes according chr1, "
                    "chr2, ... to avoid potential errors.")
        newchr = pd.Categorical(ds, categories=ds.unique())
    return newchr

