import copy
import logging

import numpy as np
from numba import jit

from simphyni.events import *

logger = logging.getLogger(__name__)

DEFAULT_EVENTS_PROB_DICT = {'chromosomal_gain': 0.2,
                            'chromosomal_loss': 0.2,
                            'focal_loss': 0.2,
                            'tandem_duplication': 0.2,
                            'unbalanced_translocation': 0.1,
                            'balanced_translocation': 0.3,
                            'insertion': 0.3,
                            'wgd': 0.005,
                            'inversion': 0.1,
                            'breakage_fusion_bridge': 0.1}

EVENTS_DICT = {'chromosomal_gain': chromosomal_gain,
               'chromosomal_loss': chromosomal_loss,
               'focal_loss': focal_loss,
               'tandem_duplication': tandem_duplication,
               'unbalanced_translocation': unbalanced_translocation,
               'balanced_translocation': balanced_translocation,
               'wgd': wgd,
               'inversion': inversion,
               'insertion': insertion,
               'breakage_fusion_bridge': breakage_fusion_bridge}

EVENTS = list(EVENTS_DICT.keys())


def number_of_events(time, size, rate=0.02):
    return np.random.poisson(time * size * rate)


def apply_events(cn, events, p_geom=0.25):
    changed_cn = copy.deepcopy(cn)
    for event in events:
        changed_cn = EVENTS_DICT[event](changed_cn, p_geom=p_geom)
        # remove empty chromosomes
        changed_cn = [x for x in changed_cn if len(x)]
        if len(changed_cn) == 0:
            break

    return changed_cn


@jit()
def evolve_single(cn, dt=1, rate=0.02, fixed_nr_segments=None, p_geom=0.25, events_probs=None):

    if events_probs is None:
        events_probs = DEFAULT_EVENTS_PROB_DICT
    # assert len(np.setdiff1d(EVENTS, list(events_probs.keys()))) == 0, \
    #     'not all events containted in events_probs. Missing are \n{}'.format(
            # np.setdiff1d(EVENTS, list(events_probs.keys())))
    events_probs = [events_probs[x] for x in EVENTS]
    events_probs = events_probs / np.sum(events_probs)

    if len(cn) == 0:
        return [], []

    if fixed_nr_segments is not None:
        nr_segments = fixed_nr_segments
    else:
        nr_segments = np.sum([len(x) for x in cn])
    nr_events = number_of_events(dt, nr_segments, rate=rate)
    logger.debug('Number of events: {}'.format(nr_events))
    events = np.random.choice(EVENTS, p=events_probs, size=nr_events)
    changed_cn = apply_events(cn, events, p_geom=p_geom)

    return changed_cn, events


def evolve_tree(tree, diploid, rate=0.02, p_geom=0.25, 
                fixed_nr_segments=None, events_probs=None):

    CN_profiles = {}
    all_events = {}
    len_diploid = np.sum([len(x) for x in diploid])

    mrca = [x for x in tree.root.clades if x.name != 'diploid'][0]
    mrca_cn, cur_events = evolve_single(diploid, 
                                        dt=1, 
                                        rate=rate,
                                        fixed_nr_segments=len_diploid, 
                                        p_geom=p_geom,
                                        events_probs=events_probs)
    CN_profiles[mrca.name] = mrca_cn
    all_events[mrca.name] = cur_events

    clade_list = [clade for clade in tree.find_clades(
        order="preorder") if clade.name is not None and clade.name != 'diploid']

    for clade in clade_list:
        children = clade.clades
        if len(children) != 0:
            for child in children:
                logger.info('Evolving child {} with parent {}'.format(child.name, clade.name))
                cur_cn, cur_events = evolve_single(CN_profiles[clade.name], dt=child.branch_length,
                                                   rate=rate, fixed_nr_segments=fixed_nr_segments, 
                                                   p_geom=p_geom, events_probs=events_probs)
                CN_profiles[child.name] = cur_cn
                all_events[child.name] = cur_events

    return CN_profiles, all_events
