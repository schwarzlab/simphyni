import logging.config
import os

import yaml

import simphyni.events
import simphyni.evolve
import simphyni.simulate
import simphyni.tools
from simphyni.simulate import simulate_cn_tree

with open(os.path.join(os.path.dirname(__file__), '../logging_conf.yaml'), 'rt') as f:
    config = yaml.safe_load(f.read())
logging.config.dictConfig(config)
