import numpy as np

from simphyni.evolve import evolve_tree
from simphyni.tools import cn_profile_from_simulation, create_diploid
from simphyni.trees import generate_tree


def simulate_cn_tree(n_leaves, rate=0.02, p_geom=0.25, events_probs=None, n_segs_per_chrom=10, n_chroms=20):

    tree = generate_tree(n_leaves)

    diploid = create_diploid(n_chroms=n_chroms, n_segs_per_chrom=n_segs_per_chrom, separate_haplotypes=True)
    len_diploid = np.sum([len(x) for x in diploid])

    CN_profiles, all_events = evolve_tree(tree, diploid, 
                                          rate=rate, p_geom=p_geom, 
                                          fixed_nr_segments=len_diploid,
                                          events_probs=events_probs)

    final_df = cn_profile_from_simulation(
        CN_profiles.values(), diploid, names=CN_profiles.keys(), cap_maxcn=None)

    for clade in tree.find_clades():
        clade.branch_length = len(all_events.get(clade.name, []))

    return tree, final_df, all_events
